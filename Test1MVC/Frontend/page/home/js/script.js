var tituloPagina = document.querySelector("html").getAttribute("data-page");
console.dir(tituloPagina);

function validar() {
    var Formulario = document.querySelector("[data-widget='booking-form']");
    var fechaEntrada = document.querySelector("[data-hook='inputDateIn']");
    var fechaSalida = document.querySelector("[data-hook='inputDateOut']");
    var numeroAdultos = document.querySelector("[data-hook='inputNumberAdult']");
    var numeroMenores = document.querySelector("[data-hook='inputNumberMinor']");
    validarFechaEntrada(fechaEntrada);
    validarFechaSalida(fechaEntrada, fechaSalida);
    validarNumeroAdultos(numeroAdultos);
    validarNumeroMenores(numeroMenores);
}

function validarFechaEntrada(fechaEntrada) {

    console.log(fechaEntrada.value);

    var dateIn = new Date(fechaEntrada.value);
    var today = new Date();
    today.setMilliseconds(0);
    today.setMinutes(0);
    today.setHours(0);
    var difDays = dateIn.getTime() - today.getTime();
    var countDays = Math.round(difDays / (1000 * 60 * 60 * 24));

    if (dateIn.getTime() < today.getTime()) {
        alert("La fecha de entrada no debe ser posterior a hoy");
        event.preventDefault();
    } else if (countDays > 180) {
        alert("La fecha de entrada no puede ser posterior a 6 meses");
        event.preventDefault();
    } else if (isNaN(dateIn)) {
        alert("La fecha de entrada no puede estar vacia");
        event.preventDefault();
    }
}

function validarFechaSalida(fechaEntrada, fechaSalida) {

    var dateIn = new Date(fechaEntrada.value);
    var dateOut = new Date(fechaSalida.value);

    var difDays = dateOut.getTime() - dateIn.getTime();
    var countDays = Math.round(difDays / (1000 * 60 * 60 * 24));

    if (countDays < 1) {
        alert("La fecha de salida debe ser posterior a la fecha de entrada");
        event.preventDefault();
    } else if (countDays > 180) {
        alert("La fecha de salida no puede ser posterior a 6 meses de la fecha de entrada");
        event.preventDefault();
    } else if (isNaN(dateIn)) {
        alert("Fecha de entrada no puede estar vacia");
        event.preventDefault();
    } else if (isNaN(dateOut)) {
        alert("Fecha de salida no puede estar vacia");
        event.preventDefault();
    }
}

function validarNumeroAdultos(numeroAdultos) {

    var numberAdult = numeroAdultos.value;

    if (numberAdult === "") {
        alert("El campo número de adultos no puede estar vacío");
        event.preventDefault();
    } else if (numberAdult <= 0) {
        alert("El número de adultos no puede ser 0 o menor");
        event.preventDefault();
    } else if (numberAdult > 10) {
        alert("El número de adultos no puede ser mayor de 10");
        event.preventDefault();
    }
}

function validarNumeroMenores(numeroMenores) {

    var numberMinor = numeroMenores.value;
    if (numberMinor < 0) {
        alert("El número de menores no puede ser menor a 0");
        event.preventDefault();
    } else if (numberMinor > 10) {
        alert("El número de menores no puede ser mayor de 10");
        event.preventDefault();
    }
}

function acceptCookies(){
    var cookiesAcc = document.querySelector("[data-hook='cookies-warning']");
    Cookies.set('USE_COOKIES', '1',  { expires: 7 });
    cookiesAcc.style.display = "none";

}

function denyCookies(){
    var cookiesDeny = document.querySelector("[data-hook='cookies-warning']");
    
    Cookies.set('USE_COOKIES', '0');
    cookiesDeny.style.display = "none";
}

var initPage_onDomContentLoaded = function () {
    var cookiesDeny = document.querySelector("[data-hook='cookies-warning']");
    if(Cookies.get('USE_COOKIES')){
        cookiesDeny.style.display = "none";
    }else{
        cookiesDeny.style.display = "block";
    }
}

document.addEventListener("DOMContentLoaded",initPage_onDomContentLoaded);

