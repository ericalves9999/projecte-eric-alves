var tituloPagina = document.querySelector("html").getAttribute("data-page");
console.dir(tituloPagina);

function validar() {
    var Formulario = document.querySelector("[data-widget='contact-form']");
    var inputNombre = document.querySelector("[data-hook='inputFirstName']");
    var inputMail = document.querySelector("[data-hook='inputEmail']");
    var inputMensaje = document.querySelector("[data-hook='inputBodyMessage']");
    validarNombre(inputNombre);
    validarMail(inputMail);
    validarMensaje(inputMensaje);
}

function validarNombre(inputNombre) {

    var nombre = inputNombre.value;
    var longitudNombre = nombre.length;

    if (longitudNombre === 0) {
        alert("El nombre no puede estar vacío");
        event.preventDefault()
    } else if (longitudNombre < 1 || longitudNombre > 80) {
        alert("El campo Nombre tiene que tener entre 1 y 80 carácteres.");
        event.preventDefault();
    }
}

function validarMail(inputMail) {

    var mail = inputMail.value;
    var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

    if (mail === "") {
        alert("El email no puede estar vacío")
        event.preventDefault();
    } else if (emailRegex.test(mail) === false) {
        alert("La dirección de email es incorrecta.");
        event.preventDefault();
    }
}

function validarMensaje(inputMensaje) {

    var mensaje = inputMensaje.value;
    console.dir(mensaje);
    var longitudMensaje = mensaje.length;

    if (longitudMensaje === 0) {
        alert("La consulta no puede estar vacia.");
        event.preventDefault();
    } else if (longitudMensaje < 1 || longitudMensaje > 2000) {
        alert("El campo cuerpo mensaje tiene que tener entre 1 y 2000 carácteres.");
        event.preventDefault();
    }
}