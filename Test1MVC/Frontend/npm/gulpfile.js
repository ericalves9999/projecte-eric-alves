'use strict';

var
	gulp = require('gulp'),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	uglifyJs = require('gulp-uglify'),
	uglifyCss = require('gulp-csso')
;


function css(done)
{
	
	gulp
		.src([
			'../shared/css/general.css',
			'../shared/css/layout.css',
			'../page/about/css/style.css',
			'../page/booking/css/style.css',
			'../page/bookingCompleted/css/style.css',
			'../page/bookingInProcess/css/stye.css',
			'../page/contact/css/style.css',
			'../page/cookieUsage/css/style.css',
			'../page/home/css/style.css',
			'../page/hotel/css/style.css',
			'../page/personalDatUsage/css/style.css',
			'../page/room/css/style.css',
			'../page/search/css/style.css',
			'../page/thanksForContactingUs/css/style.css'
		], {allowEmpty: true})	
		.pipe(concat('style.css'))
		.pipe(gulp.dest('../../public/css'))
		.pipe(rename('style.min.css'))
		.pipe(uglifyCss())
		.pipe(gulp.dest('../../public/css'))
	;

	done();
}
function img(done)
{
	gulp
		.src([
			'../shared/img/**'
		])
		.pipe(gulp.dest('../../public/img/'))
	;

	done();
}
function font(done)
{
	gulp
		.src([
			'../shared/font/**'
		])
		.pipe(gulp.dest('../../public/font/'))
	;

	done();
}


exports.default = gulp.parallel(css, img, font);
