var initPage_onDomContentLoaded = function () {

var upperButton = document.querySelector("[data-hook='scroll-to-top']");
console.dir(upperButton);
var scrollUp = function () {
    window.scrollTo(0, 0);
}
upperButton.addEventListener("click", scrollUp);

var hiddenButton = function () {
    if (window.scrollY === 0) {
        upperButton.style.display = "none";
    } else {
        upperButton.style.display = "block";
    }
}
window.addEventListener("scroll", hiddenButton);
}